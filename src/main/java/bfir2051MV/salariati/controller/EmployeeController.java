package bfir2051MV.salariati.controller;

import java.util.List;

import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;
import bfir2051MV.salariati.repository.interfaces.EmployeeRepositoryInterface;

public class EmployeeController {
	
	private EmployeeRepositoryInterface employeeRepository;
	
	public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
		this.employeeRepository = employeeRepository;
	}
	
	public void addEmployee(Employee employee) {
		employeeRepository.addEmployee(employee);
	}
	
	public List<Employee> getEmployeesList() {
		return employeeRepository.getEmployeeList();
	}

	public void modifyEmployee(String cnp, DidacticFunction didacticFunction) {
	    Employee oldEmployee = employeeRepository.findEmployeeByCNP(cnp);
	    Employee newEmployee = new Employee(oldEmployee.getLastName(),oldEmployee.getCnp(),oldEmployee.getData_nasterii(),didacticFunction,oldEmployee.getSalary());
	    employeeRepository.modifyEmployee(oldEmployee,newEmployee);
	}

	public List<Employee> getSortedEmployeesList(){
		return employeeRepository.getSortedEmployeesListBySalaryAndAge();
	}
	
}
