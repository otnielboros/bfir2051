package bfir2051MV.salariati.validator;

import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;

public class EmployeeValidator {
	
	public EmployeeValidator(){}

	public boolean isValid(Employee employee) {
		boolean isLastNameValid  = employee.getLastName().matches("[a-zA-Z]+") && (employee.getLastName().length() > 2 && employee.getLastName().length()<=50);
		boolean isCNPValid       = employee.getCnp().matches("[0-9]+") && (employee.getCnp().length() == 13);
		boolean isFunctionValid  = employee.getFunction().equals(DidacticFunction.ASISTENT) ||
								   employee.getFunction().equals(DidacticFunction.LECTURER) ||
								   employee.getFunction().equals(DidacticFunction.TEACHER);
		boolean isSalaryValid    = String.valueOf(employee.getSalary()).matches("[0-9]+") && (String.valueOf(employee.getSalary()).length() >= 1) && (employee.getSalary() > 0);
		
		return isLastNameValid && isCNPValid && isFunctionValid && isSalaryValid;
	}

	
}
