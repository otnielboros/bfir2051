package bfir2051MV.salariati.main;

import bfir2051MV.salariati.model.Employee;
import bfir2051MV.salariati.repository.implementations.EmployeeRepositoryImpl;
import bfir2051MV.salariati.repository.interfaces.EmployeeRepositoryInterface;
import bfir2051MV.salariati.controller.EmployeeController;
import bfir2051MV.salariati.enumeration.DidacticFunction;
import edu.emory.mathcs.backport.java.util.concurrent.ExecutorService;

import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {

	static EmployeeRepositoryInterface employeesRepository = new EmployeeRepositoryImpl();
	static EmployeeController employeeController = new EmployeeController(employeesRepository);
	static Scanner scanner=new Scanner(System.in);

	private static void showMenu(){
		System.out.println("1.Adaugare angajat nou");
		System.out.println("2.Modificare functie didactica");
		System.out.println("3.Afisare salariati sortati");
		System.out.println("4.Stop");
		System.out.println("Your option:");
	}

	private static void newEmployee(){
		scanner.nextLine();
		String nume,cnp;
		int salar;
		DidacticFunction functie_didactica;
		Date data_nasterii;
		System.out.println("Nume si prenume:");
		nume = scanner.nextLine();
		System.out.println("CNP:");
		cnp=scanner.nextLine();
		System.out.println("Data nasterii(MM/DD/YY):");
		data_nasterii = new Date(scanner.nextLine());
		System.out.println("Functie didactica(TEACHER,ASISTENT,LECTURER):");
		functie_didactica=DidacticFunction.valueOf(scanner.nextLine().toUpperCase());
		System.out.println("Salar:");
		salar = scanner.nextInt();
		Employee employee = new Employee(nume,cnp,data_nasterii,functie_didactica,salar);
		employeeController.addEmployee(employee);
	}

	private static void updateDidacticFunction(){
		scanner.nextLine();
		String cnp;
		DidacticFunction didacticFunction;
		System.out.println("CNP:");
		cnp	= scanner.nextLine();
		System.out.println("Noua functie didactica(TEACHER,ASISTENT,LECTURER):");
		didacticFunction = DidacticFunction.valueOf(scanner.nextLine().toUpperCase());
		employeeController.modifyEmployee(cnp,didacticFunction);

	}

	private static void showSorted(){
		for(Employee _employee : employeeController.getSortedEmployeesList())
			System.out.println(_employee.toString());
	}
	
	public static void main(String[] args) {
		int option;
		while(true){
			showMenu();
			option = scanner.nextInt();
			switch(option){
				case 1:
					newEmployee();
					break;
				case 2:
					updateDidacticFunction();
					break;
				case 3:
					showSorted();
					break;
			}
			if(option==4)
				break;
		}
	}

}
