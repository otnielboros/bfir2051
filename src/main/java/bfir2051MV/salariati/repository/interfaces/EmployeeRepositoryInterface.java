package bfir2051MV.salariati.repository.interfaces;

import java.util.List;
import bfir2051MV.salariati.model.Employee;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee);
	boolean modifyEmployee(Employee oldEmployee, Employee newEmployee);
	List<Employee> getEmployeeList();
	List<Employee> getSortedEmployeesListBySalaryAndAge();
	Employee findEmployeeByCNP(String cnp);


}
