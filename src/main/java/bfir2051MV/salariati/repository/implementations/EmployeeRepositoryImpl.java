package bfir2051MV.salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import bfir2051MV.salariati.exception.EmployeeException;

import bfir2051MV.salariati.model.Employee;

import bfir2051MV.salariati.repository.interfaces.EmployeeRepositoryInterface;
import bfir2051MV.salariati.validator.EmployeeValidator;

public class EmployeeRepositoryImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "src/main/resources/employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	private void prepareFile(){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(employeeDBFile);
			writer.print("");
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	private void addWhatRemains(int i,List<Employee> employees){
		while(i<employees.size()){
			addEmployee(employees.get(i));
			i++;
		}
	}

	@Override
	public boolean modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		boolean result = false; //1
		if(!employeeValidator.isValid(newEmployee)) //2
			return result; //3
		else {
			List<Employee> employees = getEmployeeList(); //4
			prepareFile(); //5
			int i = 0; //6
			while(i<employees.size()){ //7
				Employee employee = employees.get(i);//8
				if(employee.getCnp().equals(oldEmployee.getCnp())) { //9
					employee = newEmployee; //10
					result = true; //11
				}
				addEmployee(employee); //12
				if(result == true){//13
					addWhatRemains(++i,employees);//14
					break;//14    //consideram o instructiune comasata
				}
				i++;//15
			}
		}
		return result; //16
	} //17

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = (Employee) Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("File not found exception: " + e);
		} catch (IOException e) {
			System.err.println("IOException: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return employeeList;
	}

	@Override
	public List<Employee> getSortedEmployeesListBySalaryAndAge() {
		List<Employee> employees = getEmployeeList();
		return employees.stream().sorted((o1, o2)->{
			if(o1.getSalary()<o2.getSalary())
				return 1;
			else
				if(o1.getSalary()>o2.getSalary())
					return -1;
				else{
					if(o1.getData_nasterii().after(o2.getData_nasterii()))
						return 1;
					else
						return -1;
				}
		}).collect(Collectors.toList());
	}

	@Override
	public Employee findEmployeeByCNP(String cnp) {
		List<Employee> employees = getEmployeeList();
		for(Employee employee:employees){
			if(employee.getCnp().equals(cnp))
				return employee;
		}
		return null;
	}


}
