package bfir2051MV.salariati.repository.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bfir2051MV.salariati.enumeration.DidacticFunction;

import bfir2051MV.salariati.model.Employee;
import bfir2051MV.salariati.repository.interfaces.EmployeeRepositoryInterface;
import bfir2051MV.salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Ionel   = new Employee("Pacuraru", "1234567890876", new Date(),DidacticFunction.ASISTENT, 2500);
		Employee Mihai   = new Employee("Dumitrescu", "1234567890876",new Date(), DidacticFunction.LECTURER, 2500);
		Employee Ionela  = new Employee("Ionescu", "1234567890876",new Date(), DidacticFunction.LECTURER, 2500);
		Employee Mihaela = new Employee("Pacuraru", "1234567890876", new Date(),DidacticFunction.ASISTENT, 2500);
		Employee Vasile  = new Employee("Georgescu", "1234567890876", new Date(),DidacticFunction.TEACHER,  2500);
		Employee Marin   = new Employee("Puscas", "1234567890876",new Date(), DidacticFunction.TEACHER,  2500);
		
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}


	@Override
	public boolean modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public List<Employee> getSortedEmployeesListBySalaryAndAge() {
		return null;
	}

	@Override
	public Employee findEmployeeByCNP(String cnp) {
		return null;
	}

}
