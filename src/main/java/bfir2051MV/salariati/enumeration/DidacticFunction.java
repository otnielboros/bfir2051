package bfir2051MV.salariati.enumeration;

public enum DidacticFunction {
	ASISTENT, LECTURER, TEACHER;
}
