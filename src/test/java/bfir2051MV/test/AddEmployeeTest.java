package bfir2051MV.test;

import org.junit.Before;
import org.junit.Test;
import bfir2051MV.salariati.controller.EmployeeController;
import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;
import bfir2051MV.salariati.repository.interfaces.EmployeeRepositoryInterface;
import bfir2051MV.salariati.repository.mock.EmployeeMock;
import bfir2051MV.salariati.validator.EmployeeValidator;

import java.util.Date;

import static org.junit.Assert.*;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMock();
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();
	}
	
	@Test
	public void testRepositoryMock() {
		assertFalse(controller.getEmployeesList().isEmpty());
		assertEquals(6, controller.getEmployeesList().size());
	}
	
	@Test
	public void testAddNewEmployee() {
		Employee newEmployee = new Employee("ValidLastName", "1910509055057",new Date() ,DidacticFunction.ASISTENT, 3000);
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(7, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

}
