package bfir2051MV.salariati.repository.implementations;

import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;
import bfir2051MV.salariati.validator.EmployeeValidator;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

import static org.junit.Assert.*;

public class EmployeeRepositoryImplTest2 {

    private String employeeDBFile;
    private EmployeeRepositoryImpl repository;

    @Before
    public void setUp(){
        employeeDBFile = "src/main/resources/employees.txt";
        repository = new EmployeeRepositoryImpl();
        prepareFile();
    }

    private void prepareFile(){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(employeeDBFile);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testModifyEmployee(){
        Employee oldEmployee = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);

        Employee newEmployee2 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.TEACHER,2000);
        assert(repository.modifyEmployee(oldEmployee,newEmployee2) == false);
    }

    @Test
    public void testModifyEmployee1(){
        Employee oldEmployee = new Employee();
        Employee newEmployee = new Employee();
        newEmployee.setCnp("197");
        assert(repository.modifyEmployee(oldEmployee,newEmployee) == false);
    }

    @Test
    public void testModifyEmployee2(){
        prepareFile();
        Employee employee1 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        repository.addEmployee(employee1);

        Employee newEmployee2 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.TEACHER,2000);
        assert(repository.modifyEmployee(employee1,newEmployee2) == true);
    }

    @Test
    public void testModifyEmployee3(){
        prepareFile();
        Employee employee1 = new Employee("Boros","1971022055086",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        Employee employee2 = new Employee("Chise","1971022055088",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        repository.addEmployee(employee1);
        repository.addEmployee(employee2);

        Employee oldEmployee = new Employee();
        oldEmployee.setCnp("1971022055085");
        Employee newEmployee3 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.TEACHER,2000);
        assert(repository.modifyEmployee(oldEmployee,newEmployee3) == false);
    }

    @Test
    public void testModifyEmployee4(){
        prepareFile();
        Employee employee1 = new Employee("Boros","1971022055086",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        Employee employee2 = new Employee("Chise","1971022055088",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        Employee employee3 = new Employee("Gheorghe","1971022055085",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        Employee employee4 = new Employee("Burlacu","1971022055089",new Date("22/10/1997"), DidacticFunction.LECTURER,2000);
        repository.addEmployee(employee1);
        repository.addEmployee(employee2);
        repository.addEmployee(employee3);
        repository.addEmployee(employee4);

        Employee newEmployee4 = new Employee("Gheorghe","1971022055085",new Date("22/10/1997"), DidacticFunction.TEACHER,2000);
        assert(repository.modifyEmployee(employee3,newEmployee4) == true);
    }
}