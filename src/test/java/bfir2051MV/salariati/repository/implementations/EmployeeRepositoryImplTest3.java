package bfir2051MV.salariati.repository.implementations;

import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class EmployeeRepositoryImplTest3 {

    private String employeeDBFile;
    private EmployeeRepositoryImpl repository;
    private List<Employee> sortedEmployees;

    @Before
    public void setUp(){
        employeeDBFile = "src/main/resources/employees.txt";
        repository = new EmployeeRepositoryImpl();
        prepareFile();
        repository.addEmployee(new Employee("Florin","1931022655085",new Date("10/22/1997"), DidacticFunction.ASISTENT,20000));
        repository.addEmployee(new Employee("Otniel","1931022655086",new Date("10/22/1997"), DidacticFunction.ASISTENT,40000));
        repository.addEmployee(new Employee("Bogdan","1931022655087",new Date("9/22/1997"), DidacticFunction.ASISTENT,20000));
        sortedEmployees = repository.getSortedEmployeesListBySalaryAndAge();
    }

    private void prepareFile(){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(employeeDBFile);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testMethod1(){
        sortedEmployees = repository.getSortedEmployeesListBySalaryAndAge();
        assert(sortedEmployees.get(0).getCnp().equals("1931022655086"));
    }

    @Test
    public void testMethod2(){
        sortedEmployees = repository.getSortedEmployeesListBySalaryAndAge();
        assert(sortedEmployees.get(2).getCnp().equals("1931022655087") == false);
    }
}