package bfir2051MV.salariati.repository.implementations;

import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class EmployeeRepositoryImplTest {
    private EmployeeRepositoryImpl repository;

    @Before
    public void setUp(){
        repository = new EmployeeRepositoryImpl();
    }
    @Test
    public void testAddEmployee1(){
        Employee employee1 = new Employee("Popescu","1931022655085",new Date("10/22/1997"), DidacticFunction.TEACHER,200000);
        assertTrue(repository.addEmployee(employee1)==true);
    }

    @Test
    public void testAddEmployee2(){
        Employee employee2 = new Employee("Popescu","193",new Date("10/22/1997"), DidacticFunction.TEACHER,200000);
        assertTrue(repository.addEmployee(employee2)==false);
    }

    @Test
    public void testAddEmployee3(){
        Employee employee3 = new Employee("Popescu5","1931022655085",new Date("10/22/1997"), DidacticFunction.TEACHER,200000);
        assertTrue(repository.addEmployee(employee3)==false);
    }

    @Test
    public void testAddEmployee4(){
        Employee employee4 = new Employee("Popescu","134443355508",new Date("10/22/1997"), DidacticFunction.TEACHER,200000);
        assertTrue(repository.addEmployee(employee4)==false);
    }

}