package bfir2051MV.salariati.repository.implementations;

import bfir2051MV.salariati.enumeration.DidacticFunction;
import bfir2051MV.salariati.model.Employee;
import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class BigBangIntegrationTest {
    private String employeeDBFile;
    private EmployeeRepositoryImpl repository;
    private List<Employee> sortedEmployees;

    @Before
    public void setUp(){
        employeeDBFile = "src/main/resources/employees.txt";
        repository = new EmployeeRepositoryImpl();
    }

    private void prepareFile(){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(employeeDBFile);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void testA(){
        prepareFile();
        Employee employee3 = new Employee("Popescu","1931022655085",new Date("10/22/1997"), DidacticFunction.TEACHER,200000);
        assertTrue(repository.addEmployee(employee3)==true);
    }

    private void testB(){
        prepareFile();
        Employee employee1 = new Employee("Boros","1971022055086",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        Employee employee2 = new Employee("Chise","1971022055088",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        repository.addEmployee(employee1);
        repository.addEmployee(employee2);

        Employee oldEmployee = new Employee();
        oldEmployee.setCnp("1971022055085");
        Employee newEmployee3 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.TEACHER,2000);
        assert(repository.modifyEmployee(oldEmployee,newEmployee3) == false);
    }

    private void testC(){

        prepareFile();
        sortedEmployees = repository.getSortedEmployeesListBySalaryAndAge();
        assert(sortedEmployees.size()==0);

        prepareFile();
        repository.addEmployee(new Employee("Florin","1931022655085",new Date("10/22/1997"), DidacticFunction.ASISTENT,20000));
        repository.addEmployee(new Employee("Otniel","1931022655086",new Date("10/22/1997"), DidacticFunction.ASISTENT,40000));
        repository.addEmployee(new Employee("Bogdan","1931022655087",new Date("9/22/1997"), DidacticFunction.ASISTENT,20000));
        sortedEmployees = repository.getSortedEmployeesListBySalaryAndAge();
        assert(sortedEmployees.get(0).getCnp().equals("1931022655086"));

    }

    @Test
    public void testMethod(){
//1
        testA();

//2
        testB();

//3
        testC();


//        C
        sortedEmployees = repository.getSortedEmployeesListBySalaryAndAge();
        assert(sortedEmployees.get(0).getCnp().equals("1931022655086"));

//        B

        Employee employee5 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.ASISTENT,2000);
        repository.addEmployee(employee5);

        Employee newEmployee2 = new Employee("Boros","1971022055085",new Date("22/10/1997"), DidacticFunction.TEACHER,2000);
        assert(repository.modifyEmployee(employee5,newEmployee2) == true);

//        A
        Employee employee = new Employee("Popescu","1931022655085",new Date("10/22/1997"), DidacticFunction.TEACHER,200000);
        assert(repository.addEmployee(employee)==true);

    }
}