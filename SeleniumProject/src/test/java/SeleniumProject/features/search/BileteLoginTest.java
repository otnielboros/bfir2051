package SeleniumProject.features.search;

import SeleniumProject.steps.serenity.ApiSetup;
import SeleniumProject.steps.serenity.UiTestSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "login.csv")
public class BileteLoginTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    private String username, password,test;

    @Qualifier
    public String qualifier() {
        return username;
    }

    @Steps
    public ApiSetup setup;

    @Steps
    public UiTestSteps user;

    @Test
    public void check_loginFunctionality(){
        if(test.equals("login_valid"))
            user.logs_in_valid(username, password);
        else
            user.logs_in_nevalid(username,password);
    }
}
