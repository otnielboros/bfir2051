package SeleniumProject.features.search;

import SeleniumProject.steps.serenity.ApiSetup;
import SeleniumProject.steps.serenity.UiTestSteps;
import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityParameterizedRunner.class)
@UseTestDataFrom(value = "default_search.csv")
public class BileteSearchByDefaultBar {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    private String search,test;

    @Qualifier
    public String qualifier() {
        return search;
    }

    @Steps
    public ApiSetup setup;

    @Steps
    public UiTestSteps user;

    @Test
    public void check_searchDefaultFunctionality(){
        if(test.equals("search_valid"))
            user.searchByDefaultBar_valid(search);
        else
            user.searchByDefaultBar_nevalid(search);
    }
}
