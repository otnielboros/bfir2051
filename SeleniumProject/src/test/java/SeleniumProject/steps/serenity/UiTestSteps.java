package SeleniumProject.steps.serenity;


import SeleniumProject.pages.HomePage;
import SeleniumProject.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

public class UiTestSteps extends ScenarioSteps {

    LoginPage loginPage;
    HomePage homePage;

    @Step
    public void logs_in_valid(String username, String password) {
        loginPage.open();
        loginPage.login_valid(username, password);
    }

    @Step
    public void logs_in_nevalid(String username, String password){
        loginPage.open();
        loginPage.login_invalid(username,password);
    }

    @Step
    public void search_valid(String searchText){
        homePage.open();
        homePage.search_valid(searchText);
    }

    @Step
    public void search_nevalid(String searchText){
        homePage.open();
        homePage.search_nevalid(searchText);
    }

    @Step
    public void searchByDefaultBar_valid(String expectedH){
        homePage.open();
        homePage.searchDefault_valid(expectedH);
    }

    @Step
    public void searchByDefaultBar_nevalid(String expectedH){
        homePage.open();
        homePage.searchDefault_nevalid(expectedH);
    }


}


