package SeleniumProject.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.bilete.ro")
public class HomePage extends PageObject {
    // Page Elements
    @FindBy(xpath = "//*[@id=\"mainnav\"]/div[2]/div/div[5]/div/div/form/div/input")
    private WebElementFacade searchField;
    @FindBy(xpath = "//*[@id=\"mainnav\"]/div[2]/div/div[5]/div/div/form/div/button")
    private WebElementFacade searchButton;
    @FindBy(xpath = "/html/body/main/section[2]/div/div[1]/h3")
    private WebElementFacade succesfulSearch;
    @FindBy(xpath = "/html/body/main/section[2]/div")
    private WebElementFacade unsuccesfulSearch;
    @FindBy(xpath = "//*[@id=\"mainnav\"]/div[2]/div/div[5]/div/div/div/ul/li[3]/a")
    private WebElementFacade searchBarElement;
    @FindBy(xpath = "/html/body/main/section[1]/div/div[1]/h1")
    private WebElementFacade searchResult;
    @FindBy(xpath = "//*[@id=\"mainnav\"]/div[2]/div/div[5]/div/div/div/ul/li[1]/a")
    private WebElementFacade searchBarElement2;

    public void search_valid(String searchString) {
        waitFor(searchField);
        typeInto(searchField, searchString);
        waitFor(searchButton);
        clickOn(searchButton);
        waitFor(succesfulSearch);
    }

    public void search_nevalid(String searchString){
        waitFor(searchField);
        typeInto(searchField, searchString);
        waitFor(searchButton);
        clickOn(searchButton);
        waitFor(unsuccesfulSearch);
    }

    public void searchDefault_valid(String expectedH1){
        waitFor(searchBarElement);
        clickOn(searchBarElement);
        waitFor(searchResult);
        assert (searchResult.getText().equals(expectedH1));
    }

    public void searchDefault_nevalid(String expectedH1){
        waitFor(searchBarElement2);
        clickOn(searchBarElement2);
        waitFor(searchResult);
        assert (!searchResult.getText().equals(expectedH1));
    }

}
