package SeleniumProject.pages;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.bilete.ro/mvc/Account/Login")
public class LoginPage extends PageObject {

    // Page Elements
    @FindBy(id = "Email")
    private WebElementFacade usernameField;
    @FindBy(id = "Password")
    private WebElementFacade passwordField;
    @FindBy(xpath = "/html/body/main/div[2]/div[2]/section/form/div[5]/div/input")
    private WebElementFacade logInButton;
    @FindBy(xpath = "//*[@id=\"logoutForm\"]/a")
    private WebElementFacade headerDiv;
    @FindBy(xpath = "/html/body/main/div[2]/div[2]/section/form/div[1]/div/div/ul/li")
    private WebElementFacade invalid;

    public void login_valid(String username, String password) {
        waitFor(usernameField);
        typeInto(usernameField, username);
        typeInto(passwordField, password);
        clickOn(logInButton);
        waitFor(headerDiv);
    }

    public void login_invalid(String username, String password){
        waitFor(usernameField);
        typeInto(usernameField, username);
        typeInto(passwordField, password);
        clickOn(logInButton);
        waitFor(invalid);
    }
}
